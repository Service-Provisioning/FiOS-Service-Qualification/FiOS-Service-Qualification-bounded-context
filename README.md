# DDD-Service-Catalog-Template

DDD Service Catalog Organizational Directory Structure Template.

```mermaid
graph TD
A(Domain)
A -->B(subdomain)
B --> C(Bounded_Context-Template)
C --> D(model_artifacts)
C --> F(src)
C --> G(ubiquitous-language)
D --> J(context-map)
D --> k(data-model)
D --> L(misc)
D --> M(services.yml)
D --> N(monolith)
D --> O(wsdl)
D --> P(blupeprint)
D --> Q(events)
```
**DDD Service Catalog Base Folder Structure**
-----------------------------------------
All Bounded Contexts Repos will have the following Folder Structure.

# DDD Service Catalog Base Folder Structure

*  **model_artifacts**
   * **context-map** - Contains Maps of Bounded Contexts to show how they 
     should communicate amongst each other and how data should be shared 
   * **data-model** - Contains data model and Domain diagrams
   * **misc** - Any additional artifacts that help to define the Sub-Domain
   * **services.yml** - contains ReSTFUL API Specs conforming to the OpenAPI Standard that are used by the Sub-Domain
   * **monolith** - Contains reference to external monolith code repos & documents
   * **wsdl** - Contains the WSDL specs for SOAP-based APIs confprming to the SOAP Standard used by the Sub-Domain
   * **blueprint** - Contains Blueprint API specs conforming to the BluePrint API Description Language used by the Sub-Domain
   * **events** - Contains the  subdomain-events.csv file
*  **src** - Contains the source code for the Sub-Domain
*  **ubiquitous-language** - Document describing the vocabulary of the given
   Sub-Domain


 
# Representing a Monolith in the Service Catalog
* Monoliths are represented in the Service Catalog by having at least 1 entry in the SubDomain's `monolith_appendix.csv` file (located under model-artifacts/monolith folder). This file contains list of named URLs to Monolithic system code repos and supporting documents.
* A monolith-appendix file will exist under every SubDomain BoundedConext
* The monolith-appendix file will contain links to existing monolithic repos that the Dev teams own today
* When a monolith spans multiple subdomains, it must have an entry in the monolith-appendix file for every SubDomain Bounded-Context it has impacts to.
* A monolith is considered “loaded in the catalog” if it has at least a single entry in the monolith-appendix file and shows up in the DDD Catalog UI.

**Additional Clarifications on What will be Qualified as a Monolith**
1. All directional IEN Systems to be loaded into the catalog will be categorized into 4 Categories
   - RestFul - OAS Specs (.yml)
   - SOAP - wsdl specs (.wsdl)
   - BluePrint - (.apib)
   - monoliths
2. If an IEN Directional System does cannot provide any ReSTFul, SOAP, or BluePrint specs, they will be considered a monolith and represented as a Monolith in the DDD Service Catalog. Enablers will be created by the Architecture Review team where service decomposition is deemed applicable.

**Vendor Products are Represented as Monoliths in the DDD Catalog**
1. Vendor Products will be treated as a Monolith and handled the same way as Monoliths in the DDD Service Catalog.
2. Vendor Products must have at least 1 row representing it in the Monolith table in the DDD UI.
3. If the Vendor Product spans multiple SubDomains, like normal monoliths, it must have an entry in the monolith.csv file for each SubDomain to which it belongs to.

~~~
`Column Headings for the monolith_appendix.csv:`
`MONOLITH_NAME, TYPE, URL_NAME, URL_DESCRIPTION, AUTHOR, URL`

`Column Descriptions:`
 - MONOLITH_NAME - Name of the Monolithic application
 - TYPE - A short 1 or 2 word categorical grouping for identifying the type of data/resource this URL represent represents.  
 - URL_NAME - A short user-friendly name describing the artefact/documentation this URL points to. 
 - URL_DESCRIPTION - A 1-liner brief description explaining what the URL is pointing to or how its relevant to the monolith. 
 - AUTHOR - Point of Contact for the URL or the monolith.
 - URL - Link to the supporting material.

**Example values for the  of valid types are:**
    *  SRC_URL
    *  CONFLUENCE
    *  GITLAB_WIKI
    *  APP_URL    


**Example of a valid monolith_appendix.csv file:**
    
    MONOLITH_NAME, TYPE, URL_NAME, URL_DESCRIPTION, AUTHOR, URL
    
    Nautilus, Code Repo,**SRC_URL**,Our main code repo,nic@vz.co,https://gitlab.verizon.com/some/monolithic/gitrepo
    
    Nautilus, Documentation,**Confluence**,confluence docs,monorepo@vz.co,https://oneconfluence.verizon.com/some/monolithic/confluence
    
    Nautilus, Application,**APP_URL**,production link,support@vz.co,https://some.monolithic.repo.verizon.com
~~~

# Representing SubDomain Events in the Service Catalog
* Every model-artifacts/events directory must have subdomain-events.csv file:

~~~
**Column Headings for the subdomain-events.csv:**
`EVENT NAME, EVENT DESCRIPTION, PAYLOAD URL`

**Column Descriptions**
 - EVENT NAME - Short descriptive name of the Event
 - EVENT DESCRIPTION - One-liner description of what the event is or how it should be used.
 - PAYLOAD URL - A link to either source code or a Design artifact that shows a sample Payload for this type of Event. Examples: a link to a remote source-code repo, an xml schema in the model_artifacts folder, a json document, etc.
 ~~

Configuring hook script:
-------------------------

### To run the hook script
1. copy the code from lucid-push-rules.sh and paste the code under .git/hooks/prepare-commit-msg.sample
2. Once copied in prepare-commit-msg.sample then rename the file to `prepare-commit-msg`
3. provide executable access to prepare-commit-msg file using `chmod a+x <path/to/prepare-commit-msg>`
4. initilize the git again using `git init` this will link the current hook script to git
5. Now hooks are integrated once anything is updated/modified and we try to push the changes, then this hook script will get executed at `git commit` command level
